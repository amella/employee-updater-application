﻿Imports FirebirdSql.Data.FirebirdClient

Public Class Form1

    Private Function validarSettings()

        Dim _strUser As String = My.Settings.STR_USER
        Dim _strPass As String = My.Settings.STR_PASSWORD
        Dim _strSchema As String = My.Settings.STR_ESQUEMA
        Dim _strHost As String = My.Settings.STR_HOST
        Dim _strEmpresa As String = My.Settings.STR_EMPRESA

        Return Not (_strSchema = "" Or _strUser = "" Or _strPass = "" Or _strHost = "" Or _strEmpresa = "")

    End Function

    Private Function validarGUI()

        Dim _strRoute = tbRuta.Text.Trim()
        Dim _fecha As String = DateTimePicker1.Text().Trim()
        Dim _empresa As String = tbEmpresa.Text().Trim()

        Return Not (_strRoute = "" Or _fecha = "" Or _empresa = "")

    End Function

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub ConfiguraciónToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfiguraciónToolStripMenuItem.Click
        PreferencesDialog.Show()
    End Sub


    Private Sub btnEjecutar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEjecutar.Click

        If validarSettings() = False Then

            PreferencesDialog.Show()

        ElseIf validarGUI() = False Then

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else

            Dim str_empresa As String

            Try

                Dim _empresa As Integer = Integer.Parse(tbEmpresa.Text().Trim())


                If _empresa < 10 Then
                    str_empresa = "0" & _empresa
                Else
                    str_empresa = _empresa
                End If

            Catch ex As Exception

                MessageBox.Show("Asegurese de haber ingresado un valor numérico para la empresa.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Return

            End Try

            Try

                Dim _fecha As Date = Date.Parse(Me.DateTimePicker1.Text())
                Dim _strFecha As String = ""

                Dim _foo As Integer = _fecha.Day

                If _foo < 10 Then
                    _strFecha = "0" & _foo
                Else
                    _strFecha = _foo
                End If

                _foo = _fecha.Month

                If _foo < 10 Then
                    _strFecha = _strFecha & "0" & _foo
                Else
                    _strFecha = _strFecha & _foo
                End If

                Dim _strFoo As String = _fecha.Year

                _strFecha = _strFecha & _strFoo.Substring(2, 2) & str_empresa

                Dim service As Service = New Service()
                service.Route = tbRuta.Text.Trim()

                Dim _conectionManager As ConnectionManager = New ConnectionManager()
                _conectionManager.User = My.Settings.STR_USER
                _conectionManager.Pass = My.Settings.STR_PASSWORD
                _conectionManager.Schema = My.Settings.STR_ESQUEMA
                _conectionManager.Host = My.Settings.STR_HOST

                Dim _employeeDAO As EmployeeDAO = New EmployeeDAO()
                _employeeDAO.ConnectionManager = _conectionManager
                _employeeDAO.FechaEmpresa = _strFecha
                service.EmployeeDAO = _employeeDAO

                service.processFile()

            Catch t As FbException


                If t.ErrorCode = 335544569 Then

                    MessageBox.Show("Verifique que los objetos para la empresa existen en el esquema.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

                Else

                    Throw t

                End If


            Catch ex As Exception

                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

            End Try

        End If

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim fileDlg As New OpenFileDialog()
        fileDlg.Reset()
        fileDlg.DefaultExt = ".xlsx"
        fileDlg.Filter = "Archivos Excel | *.xlsx"
        fileDlg.Multiselect = False

        If (fileDlg.ShowDialog() = DialogResult.OK) Then

            tbRuta.Text = fileDlg.FileName
            ' Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If

    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AcercaDeToolStripMenuItem.Click
        AboutBox1.Show()
    End Sub
End Class
