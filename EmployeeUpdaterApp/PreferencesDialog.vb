﻿Imports System.Windows.Forms

Public Class PreferencesDialog

    Private Sub loadCustConnectionSettings()

        tbEsquema.Text = My.Settings.STR_ESQUEMA
        tbUsuario.Text = My.Settings.STR_USER
        tbHost.Text = My.Settings.STR_HOST
        tbPassword.Text = My.Settings.STR_PASSWORD

    End Sub

    Private Function validar()

        Dim _strEsquema As String = tbEsquema.Text.Trim()
        Dim _strHost As String = tbHost.Text.Trim()
        Dim _strUser As String = tbUsuario.Text.Trim()
        Dim _strPass As String = tbPassword.Text.Trim()

        Return Not (_strEsquema = "" Or _strHost = "" Or _strUser = "" Or _strPass = "")

    End Function

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click

        If validar() = False Then

            MessageBox.Show("Asegurese de haber ingresado los campos requeridos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        Else

            My.Settings.STR_ESQUEMA = tbEsquema.Text.Trim()
            My.Settings.STR_HOST = tbHost.Text.Trim()
            My.Settings.STR_USER = tbUsuario.Text.Trim()
            My.Settings.STR_PASSWORD = tbPassword.Text.Trim()
       
            My.Settings.Save()

            Me.Close()

        End If

    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

   

    Private Sub PreferencesDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.loadCustConnectionSettings()
    End Sub

    Private Sub btnBuscarEsquema_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarEsquema.Click

        Dim fileDlg As New OpenFileDialog()
        fileDlg.Reset()
        fileDlg.DefaultExt = ".FDB"
        fileDlg.Filter = "Archivos de Firebird|*.FDB"
        fileDlg.Multiselect = False
        If (fileDlg.ShowDialog() = DialogResult.OK) Then

            tbEsquema.Text = fileDlg.FileName
            ' Dim root As Environment.SpecialFolder = folderDlg.RootFolder

        End If

    End Sub

End Class
