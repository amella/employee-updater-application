﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports FirebirdSql.Data.FirebirdClient

Public Class Service

    Private _strRoute As String
    Private _employeeDAO As EmployeeDAO

    Public Property Route()
        Get
            Return _strRoute
        End Get
        Set(ByVal value)
            _strRoute = value
        End Set
    End Property

    Public Property EmployeeDAO()
        Get
            Return _employeeDAO
        End Get
        Set(ByVal value)
            _employeeDAO = value
        End Set
    End Property

    Public Sub processFile()

        Dim appInExcel As Excel.Application = New Excel.Application()
        Dim bookIn As Excel.Workbook = appInExcel.Workbooks.Open(_strRoute)

        appInExcel.Workbooks(1).Activate()

        Dim sheetIn1 As Excel.Worksheet = CType(bookIn.Sheets(1), Excel.Worksheet)
        sheetIn1.Select()

        Dim employees As List(Of Employee) = New List(Of Employee)

        Dim employee As Employee = Nothing

        Try

            Dim row As Integer = 2

            Dim lastRow As Integer = sheetIn1.Range("A1").SpecialCells(XlCellType.xlCellTypeLastCell).Row

            For i As Integer = row To lastRow

                employee = New Employee()

                employee._CLAVE = sheetIn1.Range("A" & i).Value
                employee._EMAIL = sheetIn1.Range("B" & i).Value
                employee._PAIS = sheetIn1.Range("C" & i).Value

                If employee._EMAIL Is Nothing Then
                    employee._EMAIL = ""
                End If

                If employee._PAIS Is Nothing Then
                    employee._PAIS = ""
                End If

                employee._CLAVE = employee._CLAVE.Trim()
                employee._EMAIL = employee._EMAIL.Trim()
                employee._PAIS = employee._PAIS.Trim()

                employee._CLAVE = employee._CLAVE.PadLeft(10)

                employees.Add(employee)

            Next

            Me._employeeDAO.updateEmployees(employees)

        Catch t As FbException

            Throw t

        Catch e As Exception

            Throw e

        Finally

            bookIn.Close(SaveChanges:=False)

            If Not appInExcel Is Nothing Then
                appInExcel.Quit()
                appInExcel = Nothing
            End If

        End Try


    End Sub

End Class
