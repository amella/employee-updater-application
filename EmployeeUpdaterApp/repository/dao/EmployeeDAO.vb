﻿Imports FirebirdSql.Data.FirebirdClient

Public Class EmployeeDAO

    Private _conectionManager As ConnectionManager
    Private _fechaEmpresa As String

    Public Property ConnectionManager()
        Get
            Return _conectionManager
        End Get
        Set(ByVal value)
            _conectionManager = value
        End Set
    End Property

    Public Property FechaEmpresa()
        Get
            Return _fechaEmpresa
        End Get
        Set(ByVal value)
            _fechaEmpresa = value
        End Set
    End Property

    Private Function getEmployeeUpdateString(ByRef employee As Employee)

        Dim foo As String = "update TB" & Me._fechaEmpresa & " SET "

        If employee._EMAIL = "" Then
            foo = foo & " EMAIL = EMAIL, "
        Else
            foo = foo & " EMAIL ='" & employee._EMAIL & "', "
        End If

        If employee._PAIS = "" Then
            foo = foo & " PAIS = PAIS "
        Else
            foo = foo & " PAIS ='" & employee._PAIS & "' "
        End If

        foo = foo & " WHERE CLAVE='" & employee._CLAVE & "'"

        Return foo

    End Function

    Private Function getEmployeeExistsString(ByRef employee As Employee)
        Dim foo As String
        foo = "select count(*) from TB" & Me._fechaEmpresa & " where CLAVE='" & employee._CLAVE & "'"
        Return foo
    End Function

    Private Sub checkExistence(ByRef employees As List(Of Employee))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlDataReader As FbDataReader = Nothing
        Dim fbCommand As FbCommand = Nothing
        Dim count As Integer = 0

        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()

            For Each employee As Employee In employees

                fbCommand = sqlConnection.CreateCommand()
                fbCommand.CommandText = getEmployeeExistsString(employee)

                sqlDataReader = fbCommand.ExecuteReader()

                While sqlDataReader.Read()

                    count = Convert.ToInt32(sqlDataReader(0))

                    If count = 0 Then

                        employee._EXISTS = False

                    Else

                        employee._EXISTS = True

                    End If

                End While

                sqlDataReader.Close()

            Next

        Catch e As FbException

            Throw e

        Finally

            If Not sqlDataReader Is Nothing Then
                sqlDataReader.Close()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

        End Try

    End Sub

    Public Sub updateEmployees(ByRef employees As List(Of Employee))

        Dim sqlConnection As FbConnection = Nothing
        Dim sqlTransaction As FbTransaction = Nothing
        Dim fbCommand As FbCommand = Nothing

        Me.checkExistence(employees)

        'realizar update
        Try

            sqlConnection = _conectionManager.getFbConnection()
            sqlConnection.Open()
            sqlTransaction = sqlConnection.BeginTransaction("UpdateTx")

            fbCommand = sqlConnection.CreateCommand()
            fbCommand.Transaction = sqlTransaction

            For Each employee As Employee In employees

                If employee._EXISTS = True Then

                    fbCommand.CommandText = getEmployeeUpdateString(employee)
                    fbCommand.ExecuteNonQuery()

                End If

            Next

            sqlTransaction.Commit()

        Catch ex As Exception

            If Not sqlTransaction Is Nothing Then
                sqlTransaction.Rollback()
            End If

            If Not sqlConnection Is Nothing Then
                sqlConnection.Close()
            End If

            Throw ex

        End Try

    End Sub

End Class
